export const USER_NOT_FOUND = '404001: USER NOT FOUND';
export const CATEGORY_NOT_FOUND = '404002: Category Not Found';
export const TODO_NOT_FOUND = '404003: TODO Not Found';

export const INVALID_CREDENTIALS = '401001: Invalid credentials';
